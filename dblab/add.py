import time
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QTableWidgetItem, QMessageBox
from connect import *

cursor, conn = connect()  

class Ui_Zhuce(object):
    def setupUi(self, Login):
        Login.setObjectName("Login")
        Login.setWindowModality(QtCore.Qt.ApplicationModal)
        Login.resize(652, 385)
        self.label = QtWidgets.QLabel(Login)
        self.label.setGeometry(QtCore.QRect(300, 20, 111, 41))
        self.label.setTextFormat(QtCore.Qt.AutoText)
        self.label.setObjectName("label")
        self.user = QtWidgets.QLabel(Login)
        self.user.setGeometry(QtCore.QRect(150, 90, 60, 41))
        self.user.setObjectName("user")
        self.password = QtWidgets.QLabel(Login)
        self.password.setGeometry(QtCore.QRect(160, 150, 60, 26))
        self.password.setObjectName("password")
        # self.identify = QtWidgets.QLabel(Login)
        # self.identify.setGeometry(QtCore.QRect(140, 180, 91, 59))
        # self.identify.setObjectName("identify")
        self.year = QtWidgets.QLabel(Login)
        self.year.setGeometry(QtCore.QRect(140, 180, 91, 59))
        self.year.setObjectName("year")
        self.name = QtWidgets.QLabel(Login)
        self.name.setGeometry(QtCore.QRect(140, 210, 110, 79))
        self.name.setObjectName("name")
        self.userline = QtWidgets.QLineEdit(Login)
        self.userline.setGeometry(QtCore.QRect(290, 100, 191, 21))
        self.userline.setObjectName("userline")
        self.pwline = QtWidgets.QLineEdit(Login)
        self.pwline.setGeometry(QtCore.QRect(290, 150, 191, 21))
        self.pwline.setEchoMode(QtWidgets.QLineEdit.Password)
        self.pwline.setObjectName("pwline")
        self.sexbox = QtWidgets.QComboBox(Login)
        self.sexbox.setGeometry(QtCore.QRect(220, 200, 51, 22))
        self.sexbox.setObjectName("sexbox")
        self.sexbox.addItem("")
        self.sexbox.addItem("")
        self.xlbox = QtWidgets.QComboBox(Login)
        self.xlbox.setGeometry(QtCore.QRect(290, 200, 61, 22))
        self.xlbox.setObjectName("xlbox")
        self.xlbox.addItem("")
        self.xlbox.addItem("")
        self.xlbox.addItem("")
        self.danwei = QtWidgets.QLineEdit(Login)
        self.danwei.setGeometry(QtCore.QRect(360, 200, 121, 22))
        self.danwei.setObjectName("danwei")

        # self.idbox = QtWidgets.QComboBox(Login)
        # self.idbox.setGeometry(QtCore.QRect(320, 200, 121, 22))
        # self.idbox.setObjectName("idbox")
        # self.idbox.addItem("")
        # self.idbox.addItem("")
        self.namebox = QtWidgets.QLineEdit(Login)
        self.namebox.setGeometry(QtCore.QRect(290, 250, 191, 21))
        self.namebox.setObjectName("namebox")
        self.loginbt = QtWidgets.QPushButton(Login)
        self.loginbt.setGeometry(QtCore.QRect(250, 320, 93, 28))
        self.loginbt.setObjectName("loginbt")
        self.exitbt = QtWidgets.QPushButton(Login)
        self.exitbt.setGeometry(QtCore.QRect(420, 320, 93, 28))
        self.exitbt.setObjectName("exitbt")
        self.retranslateUi(Login)
        QtCore.QMetaObject.connectSlotsByName(Login)
        self.exitbt.clicked.connect(self.exit)
        self.loginbt.clicked.connect(self.zhuce)

    def retranslateUi(self, Login):
        _translate = QtCore.QCoreApplication.translate #
        Login.setWindowTitle(_translate("Login", "Python图书馆管理系统"))
        self.label.setText(
            _translate("Login", "<html><head/><body><p><span style=\" font-size:14pt;\">读者注册</span></p></body></html>"))
        self.user.setText(
            _translate("Login", "<html><head/><body><p><span style=\" font-size:10pt;\">用户id</span></p></body></html>"))
        self.password.setText(
            _translate("Login", "<html><head/><body><p><span style=\" font-size:12pt;\">密码</span></p></body></html>"))
        #self.identify.setText(
        #    _translate("Login", "<html><head/><body><p><span style=\" font-size:12pt;\">身份类型</span></p></body></html>"))
        self.name.setText(
            _translate("Login", "<html><head/><body><p><span style=\" font-size:12pt;\">用户姓名</span></p></body></html>"))
        self.year.setText(
            _translate("Login", "<html><head/><body><p><span style=\" font-size:5pt;\">性别，学历，单位</span></p></body></html>"))
        self.sexbox.setItemText(0, _translate("Login", "男"))
        self.sexbox.setItemText(1, _translate("Login", "女"))
        self.xlbox.setItemText(0, _translate("Login", "本科生"))
        self.xlbox.setItemText(1, _translate("Login", "研究生"))
        self.xlbox.setItemText(2, _translate("Login", "教师"))
        self.loginbt.setText(_translate("Login", "注册"))
        self.exitbt.setText(_translate("Login", "退出"))
    def exit(self):
        rec_code = QMessageBox.question(self, "确认", "您确认要退出吗？", QMessageBox.Yes | QMessageBox.No)
        if rec_code != 65536:
            self.close() 

    def zhuce(self):
        ID = self.userline.text()
        PW = self.pwline.text()
        NM=self.namebox.text()
        DW=self.danwei.text()
        XL=self.xlbox.currentText()
        SEX=self.sexbox.currentText()
        #Type=self.idbox.currentText()
        if ID == '' or PW == ''or NM==''or DW==''or XL==''or SEX=='':
            QMessageBox.warning(self, "警告", "请输入完整信息", QMessageBox.Yes)
        else:
            sql = 'select * from readers where ID = "%s"' % (ID)  
            res=cursor.execute(sql) 
            if res:
                QMessageBox.warning(self, "警告", "用户ID已被使用", QMessageBox.Yes)
            else:
                sql = 'insert into readers() values("%s","%s","%s","%s","%s","%d","%d","%s","%d")' % (ID,NM,SEX,DW,XL,20,0,PW,0)  
                cursor.execute(sql)
                conn.commit()
                QMessageBox.information(self, "提示", "注册成功", QMessageBox.Yes)

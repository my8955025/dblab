import time
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QTableWidgetItem, QMessageBox
from connect import *
import os
from login import Ui_Login

cursor, conn = connect()  


class Ui_Zhuceadmini(object):
    def setupUi(self, Login):
        Login.setObjectName("Login")
        Login.setWindowModality(QtCore.Qt.ApplicationModal)
        Login.resize(652, 385)
        self.label = QtWidgets.QLabel(Login)
        self.label.setGeometry(QtCore.QRect(300, 20, 111, 41))
        self.label.setTextFormat(QtCore.Qt.AutoText)
        self.label.setObjectName("label")
        self.user = QtWidgets.QLabel(Login)
        self.user.setGeometry(QtCore.QRect(150, 90, 60, 41))
        self.user.setObjectName("user")
        self.password = QtWidgets.QLabel(Login)
        self.password.setGeometry(QtCore.QRect(160, 150, 60, 26))
        self.password.setObjectName("password")
        # self.identify = QtWidgets.QLabel(Login)
        # self.identify.setGeometry(QtCore.QRect(140, 180, 91, 59))
        # self.identify.setObjectName("identify")
        self.year = QtWidgets.QLabel(Login)
        self.year.setGeometry(QtCore.QRect(140, 180, 91, 59))
        self.year.setObjectName("year")
        self.name = QtWidgets.QLabel(Login)
        self.name.setGeometry(QtCore.QRect(140, 210, 110, 79))
        self.name.setObjectName("name")
        self.userline = QtWidgets.QLineEdit(Login)
        self.userline.setGeometry(QtCore.QRect(290, 100, 191, 21))
        self.userline.setObjectName("userline")
        self.pwline = QtWidgets.QLineEdit(Login)
        self.pwline.setGeometry(QtCore.QRect(290, 150, 191, 21))
        self.pwline.setEchoMode(QtWidgets.QLineEdit.Password)
        self.pwline.setObjectName("pwline")
        self.danwei = QtWidgets.QLineEdit(Login)
        self.danwei.setGeometry(QtCore.QRect(290, 200, 191, 22))
        self.danwei.setObjectName("danwei")

        # self.idbox = QtWidgets.QComboBox(Login)
        # self.idbox.setGeometry(QtCore.QRect(320, 200, 121, 22))
        # self.idbox.setObjectName("idbox")
        # self.idbox.addItem("")
        # self.idbox.addItem("")
        self.namebox = QtWidgets.QLineEdit(Login)
        self.namebox.setGeometry(QtCore.QRect(290, 250, 191, 21))
        self.namebox.setObjectName("namebox")
        self.loginbt = QtWidgets.QPushButton(Login)
        self.loginbt.setGeometry(QtCore.QRect(250, 320, 93, 28))
        self.loginbt.setObjectName("loginbt")
        self.exitbt = QtWidgets.QPushButton(Login)
        self.exitbt.setGeometry(QtCore.QRect(420, 320, 93, 28))
        self.exitbt.setObjectName("exitbt")
        self.retranslateUi(Login)
        QtCore.QMetaObject.connectSlotsByName(Login)
        self.exitbt.clicked.connect(self.exit)
        self.loginbt.clicked.connect(self.zhuce)

    def retranslateUi(self, Login):
        _translate = QtCore.QCoreApplication.translate #
        Login.setWindowTitle(_translate("Login", "Python图书馆管理系统"))
        self.label.setText(
            _translate("Login", "<html><head/><body><p><span style=\" font-size:11pt;\">管理员注册</span></p></body></html>"))
        self.user.setText(
            _translate("Login", "<html><head/><body><p><span style=\" font-size:10pt;\">用户id</span></p></body></html>"))
        self.password.setText(
            _translate("Login", "<html><head/><body><p><span style=\" font-size:12pt;\">密码</span></p></body></html>"))
        #self.identify.setText(
        #    _translate("Login", "<html><head/><body><p><span style=\" font-size:12pt;\">身份类型</span></p></body></html>"))
        self.name.setText(
            _translate("Login", "<html><head/><body><p><span style=\" font-size:10pt;\">用户姓名</span></p></body></html>"))
        self.year.setText(
            _translate("Login", "<html><head/><body><p><span style=\" font-size:10pt;\">照片地址</span></p></body></html>"))
        self.loginbt.setText(_translate("Login", "注册"))
        self.exitbt.setText(_translate("Login", "退出"))
    def exit(self):
        rec_code = QMessageBox.question(self, "确认", "您确认要退出吗？", QMessageBox.Yes | QMessageBox.No)
        if rec_code != 65536:
            self.close() 

    def zhuce(self):
        ID = self.userline.text()
        PW = self.pwline.text()
        NM=self.namebox.text()
        ADRESS=self.danwei.text()
        if ID == '' or PW == ''or NM==''or ADRESS=='':
            QMessageBox.warning(self, "警告", "请输入完整信息", QMessageBox.Yes)
        else:
            sql = 'select * from workers where ID = "%s"' % (ID)  
            res=cursor.execute(sql)
            sql='select load_file("%s")' % ADRESS
            res2=cursor.execute(sql)
            res1=cursor.fetchone()
            #print(res1[0])
            if res:
                QMessageBox.warning(self, "警告", "用户ID已被使用", QMessageBox.Yes)
            else:
                if res1[0]==None:
                    QMessageBox.warning(self, "警告", "图片地址不存在", QMessageBox.Yes)
                else:
                    cursor.callproc('add_worker_with_image', [ID, NM, PW, ADRESS])
                    conn.commit()
                    QMessageBox.information(self, "提示", "注册成功", QMessageBox.Yes)
                # sql = 'insert into readers() values("%s","%s","%s","%s","%s","%d","%d","%s","%d")' % (ID,NM,SEX,DW,XL,20,0,PW,0)  
                # cursor.execute(sql)
                # conn.commit()
                # QMessageBox.information(self, "提示", "注册成功", QMessageBox.Yes)


import sys
import time
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox
from login import Ui_Login
from addadimi import Ui_Zhuceadmini
from reader import Ui_Reader
from connect import *
#from systemadmin import Ui_systemadmin
from BookAdmin import Ui_bookadmin
from add import Ui_Zhuce

# 链接数据库
cursor, conn = connect() # 连接数据库

# 读者的界面类
class Readerui(QtWidgets.QMainWindow, Ui_Reader):
    m_id = None
    def __init__(self, parent=None,ID=None):
        super(Readerui, self).__init__(parent)
        self.m_id = ID
        self.setupUi(self)
class Zhuceadminiui(QtWidgets.QMainWindow, Ui_Zhuceadmini):
    def __init__(self, parent=None):
        super(Zhuceadminiui, self).__init__(parent)
        self.setupUi(self)
    
#  图书管理员的界面类
class bookadminui(QtWidgets.QMainWindow, Ui_bookadmin):
    m_id = None
    def __init__(self, parent=None,ID=None):
        super(bookadminui, self).__init__(parent)
        self.m_id = ID
        self.setupUi(self)

class Zhuceui(QtWidgets.QMainWindow, Ui_Zhuce):
    def __init__(self, parent=None):
        super(Zhuceui, self).__init__(parent)
        self.setupUi(self)


class MyMainForm(QMainWindow, Ui_Login):
    def __init__(self, parent=None):
        super(MyMainForm, self).__init__(parent)
        self.setupUi(self)
        self.exitbt.clicked.connect(self.exit)
        self.loginbt.clicked.connect(self.login)
        self.loginbt.setShortcut("enter")
        self.zhucebt.clicked.connect(self.zhuce)
        self.loginbt.setShortcut("zhuce")
        self.zhuceadbt.clicked.connect(self.zhuceadmini)
        self.zhuceadbt.setShortcut("zhuceadbt")

    # 退出
    def exit(self):
        rec_code = QMessageBox.question(self, "确认", "您确认要退出吗？", QMessageBox.Yes | QMessageBox.No)
        if rec_code != 65536:
            self.close()

    
    def login(self):
        ID = self.userline.text()
        PW = self.pwline.text()
        if ID == '' or PW == '':
            QMessageBox.warning(self, "警告", "请输入用户名/密码", QMessageBox.Yes)
        else:
            # 读者登录
            if self.idbox.currentText() == '读者':
                sql = 'select * from readers where ID = "%s" and password="%s"' % (ID, PW)  # 查询读者
                res = cursor.execute(sql)  # 查询结果
                if res:
                
                    
                    self.read = Readerui(ID=ID)
                    self.read.show()
                    self.close()
                else:
                    QMessageBox.warning(self, "警告", "密码错误，请重新输入！", QMessageBox.Yes)
            
            elif self.idbox.currentText() == '图书管理员':
                type = '图书管理员'
                sql = 'select * from workers where ID = "%s" and password="%s" and type="%s" ' % (ID, PW, type)
                res = cursor.execute(sql)
                # cursor.close()
                # conn.close()
                # 进行判断
                if res:
                    self.bookadmin = bookadminui(ID=ID)
                    self.bookadmin.show()
                    self.close()
                    pass
                else:
                    QMessageBox.warning(self, "警告", "密码错误，请重新输入！", QMessageBox.Yes)
    def zhuce(self):
        self.zhuce = Zhuceui()
        self.zhuce.show()
        self.close()
    def zhuceadmini(self):
        self.zhuceadmini = Zhuceadminiui()
        self.zhuceadmini.show()
        self.close()



app = QApplication(sys.argv)

myWin = MyMainForm()

myWin.show()

sys.exit(app.exec_())
